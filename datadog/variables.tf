variable "ec2_region" {
  default = "us-east-1"
}

variable "ec2_keypair_name" {
  default = "gitlabserver"
}

variable "ec2_instance_type" {
  default = "t2.micro"
}

variable "ec2_image_id" {
  default = "ami-007855ac798b5175e"
}

variable "ec2_tags" {
  default = "Descomplicando GitLab"
}

variable "ec2_instance_count" {
  default = "1"
}
